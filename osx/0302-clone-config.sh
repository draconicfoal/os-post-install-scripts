#!/bin/sh

install() {
  # clone nvim config project
  (cd $GL_DIR && git clone git@gitlab.com:rocities/nvim-config.git)
}

update() {
  (cd $GL_DIR && git pull)
}

uninstall() {
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  echo "skipping $1 on $0"
else
  echo "unsupported action $1 on $0"
fi
