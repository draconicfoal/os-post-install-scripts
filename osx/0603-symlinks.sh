#!/bin/sh

install() {
  # create symlink for .tmux.conf
  (cd $GL_DIR/tmux-config && ln -s $(pwd)/.tmux.conf ~/.tmux.conf)
}

uninstall() {
  rm -f ~/.tmux.conf
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  echo "skipping $1 on $0"
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
