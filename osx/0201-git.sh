#!/bin/sh

install() {
  brew install git -y
}

update() {
  brew upgrade git -y
}

uninstall() {
  brew uninstall git -y
}

if [ "$1" = "install" ]; then
  install
elif [ "$1" = "update" ]; then
  update
elif [ "$1" = "uninstall" ]; then
  uninstall
else
  echo "unsupported action $1 on $0"
fi
